## Web Develeoper Toolset & Utilities

### Tools for doing various webdev related things

These are tools the web developer David Higgins uses frequently.
Opinionated toolset. You probably won't use some of these.

[![...](http://f.cl.ly/items/42161a2n23111o2F2O0t/tip.png "Fund me on Gittip")](https://www.gittip.com/SoHiggo//)



###[★ Donate - gittip.com/dhig](https://www.gittip.com/SoHiggo//)

- [higg.im](http://www.higg.im/)
- [higg.tel](http://higg.tel/)
- [public@higg.im](mailto:public@higg.im)

![...](http://f.cl.ly/items/0E121z1x212X16091Y1k/s.png "...")

###[★ HTML5 video generator](http://www.iwantaneff.in/video/)
Generates all the necessary code for HTML5 video, when given only three input values (Width, MP4 URL, and Youtube Video). Uses a Youtube link as a bulletproof fallback.

###[★ Favicon grabber / cheatsheet](http://www.iwantaneff.in/favicon/)
Sometimes you just want a favicon without the fuss. Less importance is being placed on Favicons these days, and we need not care about them that much.

###[★ Flattr widget generator](http://www.iwantaneff.in/flattr/)
Creates markdown for embedding Flattr widgets. Handy for inserting into Github readmes.

###[★ JJEncoder](http://www.iwantaneff.in/jjencoder/)
Mangles Javascript up very badly. This is a fun programming tool to bewilder anyone who has the misfortune to read your source-code. Not production friendly.

###[★ Link-list to Markdown convertor](http://www.iwantaneff.in/md/)
You need a list of links converted to Markdown? This does that.

###[★ Link list opener](http://www.iwantaneff.in/opener/)
For opening a bunch of links in multiple windows. Handy for splitted archives hosted on Cyberlockers due to their file-upload size limits.

###[★ JavaScript Packify](http://www.iwantaneff.in/packer/)
You have JavaScript. You need to pack it to a really small size. This does that. Based on Ben Alman's (@cowboy) packer script.

###[★ Sitemap Generator](http://www.iwantaneff.in/sitemap/)
You have a list of URLs. You want to generate a sitemap out of them. This does that.

###[★ Textarea](http://www.iwantaneff.in/textarea/)
This is a simple textarea for typing anything you wish. Sometimes we just want a place where we can type.

###[★ Bookmarklet Generator](http://www.iwantaneff.in/bookmarklet/)
You have JavaScript. You need a bookmarklet. This does that..

###[★ Base64 Encoded URL Convertor](http://www.iwantaneff.in/dataurl/)
Converts any file you drop on the page into a Base64 Encoded URL

###[★ Entifier](http://www.iwantaneff.in/entifier/)
You have HTML code you want to place in a PRE tag element. You want it entified, so it plays nice with your PRE tag. This does that.

###[★ CSS Content Property Escaper](http://www.iwantaneff.in/escaper/)
If you dislike working with raw Unicode in your text editor, you can use this tool to escape unicode for use in the content: property in CSS.
