// When document is ready magic starts...
jQuery(document).ready( function () {
	// Attacch event handlers
	jQuery("#thing_url_input").keyup(refreshPage);
	jQuery("#thing_title_input").keyup(refreshPage);
	jQuery("[name=flattr_button_type]").click(refreshPage);


} );

/**
 * Refresh MD code and preview
 */
function refreshPage() {
	var url = jQuery("#thing_url_input").val();
	var title = jQuery("#thing_title_input").val();

	// Get choosen button type
	var button_type = jQuery("[name=flattr_button_type]:checked").val();

	updateCode(url, title, button_type);
	updatePreview(url, title, button_type);
}

/**
 * Update the preview
 */
function updatePreview(url, title, button_type) {
	var button_img_url = buttonImageUrl(button_type);

	var preview = '<a href="' + url + '" title="' + title + '">'
	            + '<img src="' + button_img_url + '" alt="Flattr This!" id="preview_image" /></a>';
	jQuery("#preview").html(preview);
}

/**
 * Update the code
 */
function updateCode(url, title, button_type) {
	var code = generateCode(url, title, button_type);
	jQuery("#output").html(code);
}

/**
 * Generate the markdown code
 */
function generateCode(url, title, button_type) {
	var button_img_url = buttonImageUrl(button_type);
	var code  = '[![Flattr Button](' + button_img_url + ' "Flattr This!")]'
	          + '(' + url + ' "' + title + '")';

	return code;
}

/**
 * Return the URL of the Flattr button image
 *
 * @param string type Type of button, e.g "compact", "normal"
 */
function buttonImageUrl(type) {
	if ( type=="compact" ) {
		// Compact button
		return 'http://api.flattr.com/button/button-compact-static-100x17.png';
	} else {
		// Normal button
		return 'http://api.flattr.com/button/button-static-50x60.png';
	}
}

